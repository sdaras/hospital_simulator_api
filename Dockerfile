FROM java:8
LABEL maintainer = "efstathiosdaras@gmail.com"
EXPOSE 8080:8080
ADD ./target/hospital_simulator_api.latest.war /usr/local/tomcat/webapps/hospital_simulator_api.latest.war
ENTRYPOINT ["java","-jar","/usr/local/tomcat/webapps/hospital_simulator_api.latest.war"]
