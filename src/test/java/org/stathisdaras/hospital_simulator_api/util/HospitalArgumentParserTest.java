package org.stathisdaras.hospital_simulator_api.util;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.stathisdaras.hospital_simulator_api.api.dto.HospitalInputArguments;
import org.stathisdaras.hospital_simulator_api.api.enumeration.Drug;
import org.stathisdaras.hospital_simulator_api.exception.ValidationException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Unit test cases for {@link HospitalArgumentParser}
 *
 * @author sdaras
 */
public class HospitalArgumentParserTest extends Assert {

	private HospitalArgumentParser parser = new HospitalArgumentParser();

	@Test(expected = ValidationException.class)
	public void resolvePatientStatuses_NonValidInput_Exception() {
		String statuses = "H,F,D,T,xX";

		parser.resolvePatientStatuses(statuses);

	}

	@Test
	public void resolveMedication_AllInput_AllDrugs() {
		String statuses = "As,An,I,P";

		List<Drug> expected = Arrays.stream(Drug.values()).collect(Collectors.toList());
		List<Drug> actual = parser.resolveMedication(statuses);

		assertEquals(expected, actual);
	}

	@Test
	public void resolveMedication_WhiteSpacesAreTrimmed() {
		String statuses = "    As,   P";

		List<Drug> expected = Lists.newArrayList(Drug.ASPIRIN, Drug.PARACETAMOL);
		List<Drug> actual = parser.resolveMedication(statuses);

		assertEquals(expected, actual);
	}

	@Test(expected = ValidationException.class)
	public void resolveMedication_InvalidInput_Exception() {
		String statuses = "As,An,Ix,P";

		 parser.resolveMedication(statuses);
	}

	@Test
	public void resolveMedication_EmptyInput_EmptyListOfDrugs() {
		String statuses = "";

		List<Drug> expected = Lists.newArrayList();
		List<Drug> actual = parser.resolveMedication(statuses);

		assertEquals(expected, actual);
	}

	@Test(expected = ValidationException.class)
	public void validateInput_NullInput_Exception() {
		String[] args = null;

		parser.validateInput(args);
	}

	@Test(expected = ValidationException.class)
	public void validateInput_EmptyInput_Exception() {
		String[] args = new String[0];

		parser.validateInput(args);
	}

	@Test
	public void validateInput_OnlyStatuses_EmptyDrugs() {
		String statuses = "H,F";
		String[] args = { statuses };

		HospitalInputArguments result = parser.validateInput(args);

		assertEquals(statuses, result.getHealthStatuses());
		assertTrue(result.getAdministeredDrugs().isEmpty());
	}

	@Test
	public void validateInput_StatusesAndDrugs_EmptyDrugs() {
		String statuses = "H,F";
		String drugs = "P";
		String[] args = { statuses, drugs };

		HospitalInputArguments result = parser.validateInput(args);

		assertEquals(statuses, result.getHealthStatuses());
		assertEquals(drugs, result.getAdministeredDrugs());
	}

}
