package org.stathisdaras.hospital_simulator_api.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.stathisdaras.hospital_simulator_api.exception.ValidationException;
import org.stathisdaras.hospital_simulator_api.util.FeatureFlag;
import org.stathisdaras.hospital_simulator_api.util.HospitalArgumentParser;
import org.stathisdaras.hospital_simulator_api.util.SpaghettiMonster;

/**
 * Unit test cases for {@link HospitalSimulationServiceImpl}
 *
 * @author sdaras
 */
@RunWith(MockitoJUnitRunner.class)
public class HospitalSimulationServiceImplTest extends Assert {

	@InjectMocks
	private HospitalSimulationServiceImpl service;

	@Spy
	private HealthStatusTransitioner statusTransitioner;

	@Spy
	private HospitalArgumentParser parser;

	@Spy
	private FeatureFlag featureFlag;

	@Before
	public void beforeEveryTest() {
		service.initialize();
		statusTransitioner.setSpaghettiMonster(new SpaghettiMonster());
	}

	@Test
	public void ifWithoutInsulin_DiabeticPatientsDie() {
		String patientStatuses = "D,D";
		String drugs = "";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:0"));
		assertTrue(outcome.contains("H:0"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:2"));
	}

	@Test
	public void ifInsulin_DiabeticPatientsSurvive() {
		String patientStatuses = "D,D";
		String drugs = "I";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:2"));
		assertTrue(outcome.contains("H:0"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:0"));
	}

	@Test
	public void ifParacetamol_PatientsWithFeverAreCured() {
		String patientStatuses = "F,D";
		String drugs = "P";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:0"));
		assertTrue(outcome.contains("H:1"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:1"));
	}

	@Test
	public void ifAspirin_PatientsWithFeverAreCured() {
		String patientStatuses = "F,X";
		String drugs = "As";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:0"));
		assertTrue(outcome.contains("H:1"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:1"));
	}

	@Test
	public void ifAntibiotic_PatientsWithTuberculosisAreCured() {
		String patientStatuses = "T";
		String drugs = "An";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:0"));
		assertTrue(outcome.contains("H:1"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:0"));
	}

	@Test
	public void ifInsulinMixedWithAntibiotic_OnlyHealthyPatientsCatchFever() {
		String patientStatuses = "H,D,T";
		String drugs = "I,An";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:1"));
		assertTrue(outcome.contains("H:1"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:1"));
		assertTrue(outcome.contains("X:0"));
	}

	@Test
	public void ifParacetamolMixedWithAspirin_PatientsAreKilled() {
		String patientStatuses = "H,D,T,F,X";
		String drugs = "P,As";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:0"));
		assertTrue(outcome.contains("H:0"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:5"));
	}

	@Test
	public void ifSpaghettiMonsterIsAwake_DeadPatientsAreResurrected() {
		String patientStatuses = "D";
		String drugs = "";


		featureFlag.isSpaghettiMonsterAwake = true;
		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		featureFlag.isSpaghettiMonsterAwake = false;

		assertTrue(outcome.contains("D:0"));
		assertTrue(outcome.contains("H:1"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:0"));

	}

	@Test
	public void ifSpaghettiMonsterIsAwake_HealthyPatientDiesAndGetsResurrected() {
		String patientStatuses = "H";
		String drugs = "P,As";

		featureFlag.isSpaghettiMonsterAwake = true;
		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		featureFlag.isSpaghettiMonsterAwake = false;

		assertTrue(outcome.contains("D:0"));
		assertTrue(outcome.contains("H:1"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:0"));

	}

	@Test
	public void ifAllHealthStatusesAllDrugs_EveryoneDies() {
		String patientStatuses = "H,D,T,F,X";
		String drugs = "P,As,I,An";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:0"));
		assertTrue(outcome.contains("H:0"));
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:0"));
		assertTrue(outcome.contains("X:5"));

	}

	@Test
	public void ifAllHealthStatusesAllDrugs_NoDeadlyCombination_StatusWillBeTransitioned() {
		String patientStatuses = "H,D,T,F,X";
		String drugs = "P,I,An,P,I,An";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertTrue(outcome.contains("D:1")); // Diabetic survived with insulin
		assertTrue(outcome.contains("H:2")); // Tuberculosis & Fever got healthy with Antibiotics & Paracetamol
		assertTrue(outcome.contains("T:0"));
		assertTrue(outcome.contains("F:1")); // Healthy got fever with Insulin & Antibiotics
		assertTrue(outcome.contains("X:1")); // Dead is still dead (This might fail if spaghetti monster is awake !)

	}

	@Test(expected = ValidationException.class)
	public void ifInvalidDrugCode_ExceptionRaised() {
		String patientStatuses = "H,D,T,F,X";
		String drugs = "Xx,P";

		service.administerDrugsToPatients(patientStatuses, drugs);
	}

	@Test(expected = ValidationException.class)
	public void ifEmptyDrugCode_ExceptionRaised() {
		String patientStatuses = "H,D,T,F,X";
		String drugs = "P,,In";

		service.administerDrugsToPatients(patientStatuses, drugs);
	}

	@Test(expected = ValidationException.class)
	public void ifInvalidHealthStatusCode_ExceptionRaised() {
		String patientStatuses = "H,Xx,T,F,X";
		String drugs = "P";

		service.administerDrugsToPatients(patientStatuses, drugs);
	}

	@Test(expected = ValidationException.class)
	public void ifEmptyHealthStatusCode_ExceptionRaised() {
		String patientStatuses = "H,D,T,F,.X";
		String drugs = "In";

		service.administerDrugsToPatients(patientStatuses, drugs);
	}

	@Test
	public void ifWithoutInsulin_DiabeticPatientsDie_CheckNumberOfOutputCodes() {
		String expected = "H:0,F:0,D:0,T:0,X:2";
		String patientStatuses = "D,D";
		String drugs = "";

		String outcome = service.administerDrugsToPatients(patientStatuses, drugs);
		assertEquals(expected, outcome);
	}

}
