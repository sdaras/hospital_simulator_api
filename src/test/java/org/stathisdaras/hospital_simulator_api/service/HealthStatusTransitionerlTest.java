package org.stathisdaras.hospital_simulator_api.service;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.stathisdaras.hospital_simulator_api.api.dto.HealthStatusTransition;
import org.stathisdaras.hospital_simulator_api.api.enumeration.Drug;
import org.stathisdaras.hospital_simulator_api.api.enumeration.HealthStatus;
import org.stathisdaras.hospital_simulator_api.util.SpaghettiMonster;

import java.util.List;
import java.util.Optional;

/**
 * Unit test cases for {@link HealthStatusTransitioner}
 *
 * @author sdaras
 */
@RunWith(MockitoJUnitRunner.class)
public class HealthStatusTransitionerlTest extends Assert {

	@InjectMocks
	private HealthStatusTransitioner transitioner;

	@Mock
	private SpaghettiMonster spaghettiMonster;

	@Test
	public void ifHealthy_NoDrugsRemainsHealthy() {
		HealthStatus currentStatus = HealthStatus.HEALTHY;
		List<Drug> administeredDrugs = Lists.newArrayList();
		HealthStatus expected = HealthStatus.HEALTHY;

		HealthStatus actual = transitioner.deductMedicationOutcome(currentStatus, administeredDrugs);

		assertEquals(expected, actual);
	}

	@Test
	public void detectEffectiveDrugCombo_IfAdministeredComboContainsEffective_True() {
		List<Drug> combo = Lists.newArrayList(Drug.PARACETAMOL, Drug.ASPIRIN, Drug.ANTIBIOTIC, Drug.INSULIN);
		List<Drug> effective = Lists.newArrayList(Drug.PARACETAMOL, Drug.ASPIRIN);

		boolean result = transitioner.effectiveDrugComboDetected(combo, effective);

		assertTrue(result);
	}

	@Test
	public void detectEffectiveDrugCombo_IfAdministeredComboPartiallyContainsEffective_False() {
		List<Drug> combo = Lists.newArrayList(Drug.PARACETAMOL, Drug.ASPIRIN);
		List<Drug> effective = Lists.newArrayList(Drug.PARACETAMOL, Drug.ANTIBIOTIC);

		boolean result = transitioner.effectiveDrugComboDetected(combo, effective);

		assertFalse(result);
	}

	@Test
	public void detectEffectiveDrugCombo_IfAdministeredComboPartiallyContainsEffective_False_2() {
		List<Drug> combo = Lists.newArrayList(Drug.PARACETAMOL, Drug.ASPIRIN);
		List<Drug> effective = Lists.newArrayList(Drug.ASPIRIN, Drug.ANTIBIOTIC);

		boolean result = transitioner.effectiveDrugComboDetected(combo, effective);

		assertFalse(result);
	}

	@Test
	public void detectEffectiveDrugCombo_IfAdministeredComboNotContainsEffective_False() {
		List<Drug> combo = Lists.newArrayList(Drug.PARACETAMOL, Drug.ASPIRIN);
		List<Drug> effective = Lists.newArrayList(Drug.INSULIN, Drug.ANTIBIOTIC);

		boolean result = transitioner.effectiveDrugComboDetected(combo, effective);

		assertFalse(result);
	}

	@Test
	public void detectEffectiveDrugComboEmptyCombo_False() {
		List<Drug> combo = Lists.newArrayList();
		List<Drug> effective = Lists.newArrayList(Drug.INSULIN, Drug.ANTIBIOTIC);

		boolean result = transitioner.effectiveDrugComboDetected(combo, effective);

		assertFalse(result);
	}


	@Test
	public void detectAnyEffectiveDrug_IfAdministeredContainedInEffective_True() {
		List<Drug> administered = Lists.newArrayList(Drug.PARACETAMOL, Drug.ASPIRIN);
		List<Drug> effective = Lists.newArrayList(Drug.ASPIRIN, Drug.ANTIBIOTIC);

		boolean result = transitioner.anyEffectiveDrugDetected(administered, effective);

		assertTrue(result);
	}

	@Test
	public void detectAnyEffectiveDrug_IfAdministeredNotContainedInEffective_False() {
		List<Drug> administered = Lists.newArrayList(Drug.PARACETAMOL, Drug.ASPIRIN);
		List<Drug> effective = Lists.newArrayList(Drug.INSULIN, Drug.ANTIBIOTIC);

		boolean result = transitioner.anyEffectiveDrugDetected(administered, effective);

		assertFalse(result);
	}

	@Test
	public void detectAnyEffectiveDrug_IfAdministeredEmpty_False() {
		List<Drug> administered = Lists.newArrayList();
		List<Drug> effective = Lists.newArrayList(Drug.INSULIN, Drug.ANTIBIOTIC);

		boolean result = transitioner.anyEffectiveDrugDetected(administered, effective);

		assertFalse(result);
	}

	@Test
	public void findPrioritizedEffectiveTransition_HighestPriorityWIllBeSelected() {
		List<Drug> drugs = Lists.newArrayList(Drug.ASPIRIN, Drug.PARACETAMOL);

		HealthStatusTransition transition1 = HealthStatusTransition.builder().priority(1).effectiveDrugs(Lists.newArrayList(Drug.ASPIRIN))
				.resultingStatus(HealthStatus.HEALTHY).build();
		HealthStatusTransition transition2 = HealthStatusTransition.builder().priority(2).effectiveDrugs(Lists.newArrayList(Drug.ASPIRIN, Drug.PARACETAMOL))
				.resultingStatus(HealthStatus.DEAD).build();

		List<HealthStatusTransition> transitions = Lists.newArrayList(
				transition1,
				transition2
		);

		Optional<HealthStatusTransition> actual = transitioner.findPrioritizedEffectiveTransition(drugs, transitions);

		assertTrue(actual.isPresent());
		assertEquals(transition1, actual.get());

	}

	@Test
	public void findPrioritizedEffectiveTransition_HighestPriorityWIllBeSelected_2() {
		List<Drug> drugs = Lists.newArrayList(Drug.ASPIRIN, Drug.PARACETAMOL);

		HealthStatusTransition transition1 = HealthStatusTransition.builder().priority(2).effectiveDrugs(Lists.newArrayList(Drug.ASPIRIN))
				.resultingStatus(HealthStatus.HEALTHY).build();
		HealthStatusTransition transition2 = HealthStatusTransition.builder().priority(1).effectiveDrugs(Lists.newArrayList(Drug.ASPIRIN, Drug.PARACETAMOL))
				.resultingStatus(HealthStatus.DEAD).build();

		List<HealthStatusTransition> transitions = Lists.newArrayList(
				transition1,
				transition2
		);

		Optional<HealthStatusTransition> actual = transitioner.findPrioritizedEffectiveTransition(drugs, transitions);

		assertTrue(actual.isPresent());
		assertEquals(transition2, actual.get());

	}

}
