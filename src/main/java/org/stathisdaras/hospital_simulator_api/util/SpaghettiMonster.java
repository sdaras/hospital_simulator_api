package org.stathisdaras.hospital_simulator_api.util;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * The Flying Flying Spaghetti Monster shows his noodly power
 * and resurrects a dead patient one time in a million (Dead becomes Healthy).
 *
 * @author sdaras
 */
@Component
public class SpaghettiMonster {
	private static final int OCCURRENCES_BEFORE_RESURRECTION = 1_000_000;

	/**
	 * Deducts by the use of random math function if it is this one in a million times that the spaghetti monster
	 * is awake and ready to perform the miracle of resurrection )
	 *
	 * @return
	 * 		true if the monster is awake
	 */
	public boolean isAwake() {
		return FeatureFlag.isSpaghettiMonsterAwake || new Random().nextInt(OCCURRENCES_BEFORE_RESURRECTION) == 0;
	}

}
