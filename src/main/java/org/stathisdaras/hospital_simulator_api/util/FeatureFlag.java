package org.stathisdaras.hospital_simulator_api.util;

/**
 * Helper class holding feature flags (e.g: useful for testing)
 *
 * @author sdaras
 */
public class FeatureFlag {

	/***
	 * Flag that can explicitly enable Spaghetti Monster miraculous functionality.
	 * Will be used for testing purposes.
	 */
	public static boolean isSpaghettiMonsterAwake = false;
}
