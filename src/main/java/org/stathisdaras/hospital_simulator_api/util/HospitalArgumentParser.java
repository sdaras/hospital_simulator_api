package org.stathisdaras.hospital_simulator_api.util;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.stathisdaras.hospital_simulator_api.api.dto.HospitalInputArguments;
import org.stathisdaras.hospital_simulator_api.api.enumeration.Drug;
import org.stathisdaras.hospital_simulator_api.api.enumeration.HealthStatus;
import org.stathisdaras.hospital_simulator_api.exception.ValidationException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Helper class responsible for parsing input arguments.
 * It is designed as a singleton because there is no need to create a new instance every time we need to use this class.
 * Sure it is not thread safe, but the application is single threaded.
 *
 * @author sdaras
 */
@Component
public class HospitalArgumentParser {

	private static final String COMMA_DELIMITER = ",";

	/**
	 * Converts a comma separated string of codes to a list of proper enumerations
	 *
	 * @param statuses
	 * 		string of health statuses
	 * @return
	 * 		list of health statuses
	 */
	public List<HealthStatus> resolvePatientStatuses(String statuses) {
		if (StringUtils.isEmpty(statuses.trim())) {
			return Lists.newArrayList();
		}

		return Arrays.stream(statuses.split(COMMA_DELIMITER)).map(HealthStatus::readFromStatusCode).collect(Collectors.toList());
	}

	/**
	 * Converts a comma separated string of codes to a list of proper enumerations
	 *
	 * @param drugs
	 * 		string of applied drugs
	 * @return
	 * 		list of administered drugs
	 */
	public List<Drug> resolveMedication(String drugs) {
		if (StringUtils.isEmpty(drugs.trim())) {
			return Lists.newArrayList();
		}

		return Arrays.stream(drugs.split(COMMA_DELIMITER)).map(Drug::readFromDrugCode).collect(Collectors.toList());
	}

	/**
	 * Validates user input and trasforms to helper Dto
	 *
	 * @param args
	 * 		command line arguments array
	 * @return
	 * 		Dto wrapping the application's arguments
	 */
	public HospitalInputArguments validateInput(String[] args) {
		String statuses;
		String drugs = "";

		if (ArrayUtils.isEmpty(args)) {
			throw new ValidationException("No arguments provided. At least one argument is mandatory.");
		}

		statuses = args[0];

		if (args.length > 1) {
			drugs = args[1];
		}

		return HospitalInputArguments.builder().healthStatuses(statuses).administeredDrugs(drugs).build();
	}
}
