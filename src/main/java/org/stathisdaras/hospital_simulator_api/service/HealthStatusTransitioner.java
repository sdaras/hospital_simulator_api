package org.stathisdaras.hospital_simulator_api.service;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stathisdaras.hospital_simulator_api.api.dto.HealthStatusTransition;
import org.stathisdaras.hospital_simulator_api.api.dto.HealthStatusTransitionRule;
import org.stathisdaras.hospital_simulator_api.api.enumeration.Drug;
import org.stathisdaras.hospital_simulator_api.api.enumeration.HealthStatus;
import org.stathisdaras.hospital_simulator_api.rule_engine.HealthStatusTransitionConfiguration;
import org.stathisdaras.hospital_simulator_api.util.SpaghettiMonster;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * Service class that is responsible for deducting the health status that comes up after applying effective drugs
 * to the current status of a patient
 *
 * @author sdaras
 */
@Service
public class HealthStatusTransitioner {

	/**
	 * Flying Flying Spaghetti Monster
	 */
	@Autowired
	private SpaghettiMonster spaghettiMonster;

	/**
	 * Map of health status transitions depending on the administered drugs
	 */
	private HealthStatusTransitionConfiguration config = new HealthStatusTransitionConfiguration();

	/**
	 * Deducts the health status that comes up after applying effective drugs:
	 * <p>
	 * 1. Check if global transition can be applied (e.g if a deadly combination of drugs is detected, outcome is DEAD for everybody)
	 * 2. Else, transition map is used to reach to a conclusion
	 * 3. Finally, an attempt is made to resurrect dead patients if spaghetti monster is awake
	 *
	 * @param currentStatus     current patient status
	 * @param administeredDrugs drugs that are administered to help the patient
	 * @return final status of the patient after drugs are properly administered
	 */
	public HealthStatus deductMedicationOutcome(HealthStatus currentStatus, List<Drug> administeredDrugs) {
		// First check if there is a global transition that can be applied
		Optional<HealthStatusTransition> globalTransitionOptional =
				findPrioritizedEffectiveTransition(administeredDrugs, config.getRuleForStatus(HealthStatus.ANY).getTransitions());

		// If not, deduct outcome by per status transitions
		HealthStatus outcome = globalTransitionOptional.isPresent()
				? globalTransitionOptional.get().getResultingStatus()
				: deductTransitionOutcome(administeredDrugs, config.getRuleForStatus(currentStatus));

		// Attempt resurrection
		if (HealthStatus.DEAD.equals(outcome) && spaghettiMonster.isAwake()) {
			outcome = HealthStatus.HEALTHY;
		}

		return outcome;
	}

	/**
	 * Deducts the health status that comes up after applying effective drugs.
	 *
	 * @param administeredDrugs input drugs
	 * @param rule       {@link HealthStatusTransitionRule} that eventually will be applied
	 * @return deducted health status
	 */
	private HealthStatus deductTransitionOutcome(List<Drug> administeredDrugs, HealthStatusTransitionRule rule) {
		Optional<HealthStatusTransition> transitionOptional = findPrioritizedEffectiveTransition(administeredDrugs, rule.getTransitions());

		return transitionOptional.isPresent() ? transitionOptional.get().getResultingStatus() : rule.getFallBackStatus();
	}

	/**
	 * Filters out effective possible transitions,sorts them by natural order and returns first
	 *
	 * @param administeredDrugs drugs given to patient
	 * @param transitions       possible health status transitions
	 * @return actual transition to be applied
	 */
	protected Optional<HealthStatusTransition> findPrioritizedEffectiveTransition(List<Drug> administeredDrugs, List<HealthStatusTransition> transitions) {
		return transitions.stream()
				.filter(t -> isMedicationEffective(administeredDrugs, t))
				.sorted() // WIll select the first global transition sorted by their natural order (field priority)
				.findFirst();
	}

	/**
	 * Compares administered drugs to effective drugs as defined by the {@link HealthStatusTransition}
	 *
	 * @param administeredDrugs input administeredDrugs
	 * @param transition        health status transition
	 * @return true if the administered drugs have an effect on the patient
	 */
	private boolean isMedicationEffective(List<Drug> administeredDrugs, HealthStatusTransition transition) {
		return transition.isMixDrugs()
				? effectiveDrugComboDetected(administeredDrugs, transition.getEffectiveDrugs())
				: anyEffectiveDrugDetected(administeredDrugs, transition.getEffectiveDrugs());
	}

	/**
	 * Detects if any of the administered drug can have an effect
	 *
	 * @param administeredDrugs drugs administered
	 * @param effectiveDrugs    drugs that work
	 * @return true if effective drugs are found
	 */
	protected boolean anyEffectiveDrugDetected(List<Drug> administeredDrugs, List<Drug> effectiveDrugs) {
		return effectiveDrugs.stream().anyMatch(administeredDrugs::contains);
	}

	/**
	 * Detects if the combination of the administered drug can have an effect
	 *
	 * @param administeredDrugs drugs administered
	 * @param effectiveDrugs    drugs that work
	 * @return true if effective drugs are found
	 */
	protected boolean effectiveDrugComboDetected(List<Drug> administeredDrugs, List<Drug> effectiveDrugs) {
		return !CollectionUtils.isEmpty(administeredDrugs)
				// Putting administered drugs in a set ensures that existence of every drug will be calculated once
				&& new HashSet<>(administeredDrugs).stream().filter(effectiveDrugs::contains).count() == effectiveDrugs.size();
	}

	/**
	 * Setter for testing purposes
	 *
	 * @param spaghettiMonster
	 */
	public void setSpaghettiMonster(SpaghettiMonster spaghettiMonster) {
		this.spaghettiMonster = spaghettiMonster;
	}
}
