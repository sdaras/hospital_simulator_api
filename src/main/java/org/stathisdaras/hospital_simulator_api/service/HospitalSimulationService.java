package org.stathisdaras.hospital_simulator_api.service;

/**
 * Interface defining methods that will be used as to simulate hospital functionality
 *
 * @author sdaras
 */
public interface HospitalSimulationService {

	/**
	 * Accepts input arguments and returns the result of administered drugs to patients
	 *
	 * @param statuses
	 * 		Health statuses received from input
	 * @param drugs
	 * 		Administered drugs
	 * @return
	 * 		Comma separated string with number of patients with a given state, following the format:
	 * 		F:NP,H:NP,D:NP,T:NP,X:NP
	 */
	String administerDrugsToPatients(String statuses, String drugs);
}
