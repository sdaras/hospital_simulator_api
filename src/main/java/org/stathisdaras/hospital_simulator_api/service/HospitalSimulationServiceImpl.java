package org.stathisdaras.hospital_simulator_api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stathisdaras.hospital_simulator_api.api.enumeration.Drug;
import org.stathisdaras.hospital_simulator_api.api.enumeration.HealthStatus;
import org.stathisdaras.hospital_simulator_api.util.HospitalArgumentParser;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service implementation for hospital simulation, containing method implementation for administering drugs to patients.
 *
 * @author sdaras
 */
@Service
public class HospitalSimulationServiceImpl implements HospitalSimulationService {

	/**
	 * Map that holds a count of patients per health status
	 */
	private Map<HealthStatus, Long> patientsPerStatus = new HashMap<>();

	/**
	 * Service class that is responsible for deducting the health status that comes up after applying effective drugs
	 * to the current status of a patient
	 */
	@Autowired
	private HealthStatusTransitioner statusTransitioner;

	/**
	 * Parser of input arguments
	 */
	@Autowired
	private HospitalArgumentParser parser;

	@PostConstruct
	protected void initialize() {
		HealthStatus.valuesAsList().forEach(s -> patientsPerStatus.put(s, 0L));
	}


	@Override
	public String administerDrugsToPatients(String initialPatientStatuses, String administeredDrugs) {
		List<HealthStatus> statuses = parser.resolvePatientStatuses(initialPatientStatuses);
		List<Drug> drugs = parser.resolveMedication(administeredDrugs);

		List<HealthStatus> finalPatientsStatuses = statuses.stream()
				.map(status -> statusTransitioner.deductMedicationOutcome(status, drugs))
				.collect(Collectors.toList());

		Map<HealthStatus, Long> statusesCount = countPatientsPerHealthStatus(finalPatientsStatuses);
		return formatStatusesCount(statusesCount);
	}

	/**
	 * Formats a map according to the following output format:
	 * F:NP,H:NP,D:NP,T:NP,X:NP
	 * Where
	 * - F, H, D, T, X are patients’ health status codes;
	 * - NP is a number of patients for a given state;
	 *
	 * @param statusesCount map of encounters per allowed health status
	 * @return result as a formatted string
	 */
	private String formatStatusesCount(Map<HealthStatus, Long> statusesCount) {
		return HealthStatus.valuesAsList().stream()
				.map(s -> s.getStatusCode() + ":" + statusesCount.get(s))
				.collect(Collectors.joining(","));
	}

	/**
	 * Receives a list of statuses and produces a map of encounters per allowed health status
	 *
	 * @param statuses statuses to format
	 * @return map containing how many times each health status was encountered
	 */
	private  Map<HealthStatus, Long> countPatientsPerHealthStatus(List<HealthStatus> statuses) {
		statuses.forEach(s -> {
			Long statusEncounters = patientsPerStatus.get(s);
			patientsPerStatus.put(s, ++statusEncounters);
		});
		return patientsPerStatus;
	}
}
