package org.stathisdaras.hospital_simulator_api.api.dto;

import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Data;
import org.stathisdaras.hospital_simulator_api.api.enumeration.HealthStatus;

import java.util.List;

/**
 * Dto that describes a transition rule that can be applied to a health status
 *
 * @author sdaras
 */
@Data
@Builder
public class HealthStatusTransitionRule{

	/**
	 * List of possible transitions that have an effect on the specific status
	 */
	private List<HealthStatusTransition> transitions = Lists.newArrayList();

	/**
	 * Default transitioning status, if no effective transitions can be applied
	 */
	private HealthStatus fallBackStatus;
}
