package org.stathisdaras.hospital_simulator_api.api.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Helper dto wrapping the application's arguments
 *
 * @author sdaras
 */
@Data
@Builder
public class HospitalInputArguments {

	/**
	 * List of patients' health status codes, separated by a comma
	 */
	private String healthStatuses;

	/**
	 * List of drugs codes, separated by a comma
	 */
	private String administeredDrugs;
}
