package org.stathisdaras.hospital_simulator_api.api.dto;

import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Data;
import org.stathisdaras.hospital_simulator_api.api.enumeration.Drug;
import org.stathisdaras.hospital_simulator_api.api.enumeration.HealthStatus;

import java.util.List;

/**
 * Dto that describes the transition from one health status to another
 *
 * @author sdaras
 */
@Data
@Builder
public class HealthStatusTransition implements Comparable<HealthStatusTransition>{

	/**
	 * List of drugs that have an effect on the specific status
	 */
	private List<Drug> effectiveDrugs = Lists.newArrayList();

	/**
	 * Flag denoting of the drugs need to be mixed in order to take effect
	 */
	private boolean mixDrugs;

	/**
	 * The status where the initial status will transit to, if effective drugs are administered
	 */
	private HealthStatus resultingStatus;

	/**
	 * Priority of health status transition, defining which transition should be followed in case multiple
	 * transitions can be followed by drug administration to patients
	 */
	private Integer priority;

	/**
	 * Overridden method of {@link Comparable} interface
	 * will define the natural order of transitions.
	 * Higher will be considered a transition with higher priority (integer of smaller value).
	 *
	 * E.g: a transition with priority of value 1 is of higher priority than a transition of value 2
	 *
	 * @param otherTransition
	 * 		other transition to be compared with this
	 * @return
	 * 		> 0 if this transition is of higher priority
	 * 	    < 0 if lower
	 * 	    0 if they are equal
	 */
	@Override
	public int compareTo(HealthStatusTransition otherTransition) {
		Integer priority = this.getPriority() != null ? this.getPriority() : 0;
		Integer otherPriority = otherTransition.getPriority() != null ? otherTransition.getPriority() : 0;

		return priority.compareTo(otherPriority);
	}
}
