package org.stathisdaras.hospital_simulator_api.api.enumeration;

import org.stathisdaras.hospital_simulator_api.exception.ValidationException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Enum representing all possible health statuses that a patient can go through
 *
 * @author sdaras
 */
public enum HealthStatus {
	HEALTHY("H"),
	FEVER("F"),
	DIABETES("D"),
	TUBERCULOSIS("T"),
	DEAD("X"),
	ANY("") // Support health status denoting either one of the enumerated statuses, will not be printed in the results outcome
	;

	/**
	 * The value of the health status as is allowed to be passed by in the command line arguments
	 */
	private final String statusCode;

	HealthStatus(String value) {
		this.statusCode = value;
	}


	/**
	 * Returns the drug enum value that corresponds to the drug code
	 *
	 * @param statusCode
	 * 			input health status code
	 * @return
	 * 		HealthStatus enum value that corresponds to the health status code
	 */
	public static HealthStatus readFromStatusCode(String statusCode) {
		Optional<HealthStatus> statusOptional = Arrays.stream(values()).filter(d -> d.getStatusCode().equals(statusCode.trim())).findFirst();

		if (!statusOptional.isPresent()) {
			throw new ValidationException("Invalid patient status: " + statusCode + ". Please insert comma separated status codes"
					+ " from the following options: "
					+ valuesAsList().stream()
					.map(s -> s.statusCode)
					.collect(Collectors.toList()));
		}

		return statusOptional.get();
	}

	/**
	 * Returns a list of the enumerations
	 *
	 * @return 	enum list
	 */
	public static List<HealthStatus> valuesAsList() {
		return Arrays.stream(HealthStatus.values())
				.filter(s -> !s.equals(ANY)) // ANY status code will not be included in the output
				.collect(Collectors.toList());
	}

	public String getStatusCode() {
		return statusCode;
	}
}
