package org.stathisdaras.hospital_simulator_api.api.enumeration;

import org.stathisdaras.hospital_simulator_api.exception.ValidationException;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Enum representing all possible drugs that can be administered
 *
 * @author sdaras
 */
public enum Drug {
	ASPIRIN("As"),
	ANTIBIOTIC("An"),
	INSULIN("I"),
	PARACETAMOL("P");

	/**
	 * The value of the drug as is allowed to be passed by in the command line arguments
	 */
	private final String drugCode;

	Drug(String value) {
		this.drugCode = value;
	}

	/**
	 * Returns the drug enum value that corresponds to the drug code
	 *
	 * @param drugCode
	 * 			input drug code
	 * @return
	 * 		Drug enum value that corresponds to the drug code
	 */
	public static Drug readFromDrugCode(String drugCode) {
		Optional<Drug> drugOptional = Arrays.stream(values()).filter(d -> d.getDrugCode().equals(drugCode.trim())).findFirst();

		// Drug validation against possible enumerated values
		if (!drugOptional.isPresent()) {
			throw new ValidationException("Invalid administered drug: " + drugCode + ". Please insert comma separated status codes"
					+ " from the following options: " + Arrays.stream(Drug.values())
					.map(d -> d.drugCode).collect(Collectors.toList()));
		}

		return drugOptional.get();
	}

	public String getDrugCode() {
		return drugCode;
	}
}
