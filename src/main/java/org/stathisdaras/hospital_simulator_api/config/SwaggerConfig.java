package org.stathisdaras.hospital_simulator_api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger configuration for API documentation
 *
 * @author sdaras
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(new ApiInfoBuilder()
						.title("Hospital Simulator API")
						.description("Java API simulating the future patients’ state, based on their current state and a list of drugs they take")
						.build())
				.select()
				.apis(RequestHandlerSelectors.basePackage("org.stathisdaras.hospital_simulator_api"))
				.paths(PathSelectors.any())
				.build();
	}
}