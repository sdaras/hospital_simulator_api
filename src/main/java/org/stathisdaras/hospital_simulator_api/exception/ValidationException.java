package org.stathisdaras.hospital_simulator_api.exception;

/**
 * Runtime exception that can be thrown to denote that a validation error occurred
 *
 * @author sdaras
 */
public class ValidationException extends RuntimeException {

	public ValidationException(String message) {
		super(message);
	}

}
