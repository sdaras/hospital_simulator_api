package org.stathisdaras.hospital_simulator_api.rule_engine;

import lombok.Data;
import org.stathisdaras.hospital_simulator_api.api.dto.HealthStatusTransitionRule;
import org.stathisdaras.hospital_simulator_api.api.enumeration.Drug;
import org.stathisdaras.hospital_simulator_api.api.enumeration.HealthStatus;

import java.util.Map;

/**
 * Dto containing a map of health status transitionRules.
 *
 * @author sdaras
 */
@Data
public class HealthStatusTransitionConfiguration {

	/**
	 * Transition map, every status transits to another, if effective drugs are administered
	 */
	private Map<HealthStatus, HealthStatusTransitionRule> transitionRules;

	/**
	 * During configuration construction a map of {@link HealthStatusTransitionRule} per health status is properly populated
	 * according to the hospital simulator assignment specifications.
	 * For further development and extension of the application functionality this is where transtions or rules should be added.
	 */
	public HealthStatusTransitionConfiguration() {
		// Initialize map with the given transitionRules as defined by the specs
		transitionRules = HealthStatusTransitionMapBuilder.transitionRules()

				// Paracetamol kills subject if mixed with aspirin;
				.forAllPatients().ifIsTreatedWithMixOf(Drug.PARACETAMOL, Drug.ASPIRIN).willTransitionToStatus(HealthStatus.DEAD).withPriority(1)
				.endOfTransition()
				.endOfRule()

				// If insulin is mixed with antibiotic, healthy people catch Fever
				.forPatientOfStatus(HealthStatus.HEALTHY).ifIsTreatedWithMixOf(Drug.INSULIN, Drug.ANTIBIOTIC).willTransitionToStatus(HealthStatus.FEVER)
				.endOfTransition()
				.elseWillTransitionToStatus(HealthStatus.HEALTHY)
				.endOfRule()

				// Insulin prevents diabetic subject from dying, does not cure Diabetes
				.forPatientOfStatus(HealthStatus.DIABETES).ifIsTreatedWithEitherOf(Drug.INSULIN).willTransitionToStatus(HealthStatus.DIABETES)
				.endOfTransition()
				.elseWillTransitionToStatus(HealthStatus.DEAD)
				.endOfRule()

				// Antibiotic cures Tuberculosis
				.forPatientOfStatus(HealthStatus.TUBERCULOSIS).ifIsTreatedWithEitherOf(Drug.ANTIBIOTIC).willTransitionToStatus(HealthStatus.HEALTHY)
				.endOfTransition()
				.elseWillTransitionToStatus(HealthStatus.TUBERCULOSIS)
				.endOfRule()

				// Aspirin cures Fever & Paracetamol cures Fever
				.forPatientOfStatus(HealthStatus.FEVER).ifIsTreatedWithEitherOf(Drug.ASPIRIN, Drug.PARACETAMOL).willTransitionToStatus(HealthStatus.HEALTHY)
				.endOfTransition()
				.elseWillTransitionToStatus(HealthStatus.FEVER)
				.endOfRule()

				// Dead remains dead, unless...
				.forPatientOfStatus(HealthStatus.DEAD).ifIsTreatedWithAnyDrug().willTransitionToStatus(HealthStatus.DEAD)
				.endOfTransition()
				.elseWillTransitionToStatus(HealthStatus.DEAD)
				.endOfRule()

				.endOfRules();
	}

	public HealthStatusTransitionRule getRuleForStatus(HealthStatus status) {
		return transitionRules.get(status);
	}
}
