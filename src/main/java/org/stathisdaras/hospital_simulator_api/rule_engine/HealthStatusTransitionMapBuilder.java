package org.stathisdaras.hospital_simulator_api.rule_engine;

import com.google.common.collect.Lists;
import org.stathisdaras.hospital_simulator_api.api.dto.HealthStatusTransition;
import org.stathisdaras.hospital_simulator_api.api.dto.HealthStatusTransitionRule;
import org.stathisdaras.hospital_simulator_api.api.enumeration.Drug;
import org.stathisdaras.hospital_simulator_api.api.enumeration.HealthStatus;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class defining a set of building methods aimed at helping the creation of the complex
 * {@link HealthStatusTransitionRule} and {@link HealthStatusTransition} objects that form
 * the backbone of the application's transition rule rule_engine.
 * The result of the builder is the transitionRules map that keeps track of every health status's
 * possible transitions.
 *
 * @author sdaras
 */
public class HealthStatusTransitionMapBuilder {

	/**
	 * Transition map, every status transitions to another, if effective drugs are administered
	 */
	private Map<HealthStatus, HealthStatusTransitionRule> transitionRules = new HashMap<>();

	private HealthStatus currentBuildingRuleStatus;

	private HealthStatusTransition.HealthStatusTransitionBuilder currentTransitionBuilder = HealthStatusTransition.builder();

	private HealthStatusTransitionRule currentRule = HealthStatusTransitionRule.builder().build();

	private List<HealthStatusTransition> currentTransitions = Lists.newArrayList();

	static HealthStatusTransitionMapBuilder transitionRules() {
		return new HealthStatusTransitionMapBuilder();
	}

	public HealthStatusTransitionMapBuilder forPatientOfStatus(HealthStatus status) {
		currentBuildingRuleStatus = status;

		return this;
	}

	public HealthStatusTransitionMapBuilder forAllPatients() {
		currentBuildingRuleStatus = null;

		return this;
	}

	public HealthStatusTransitionMapBuilder ifIsTreatedWithMixOf(Drug... drugs) {
		currentTransitionBuilder.mixDrugs(true)
				.effectiveDrugs(Arrays.asList(drugs));

		return this;
	}

	public HealthStatusTransitionMapBuilder ifIsTreatedWithEitherOf(Drug... drugs) {
		currentTransitionBuilder
				.effectiveDrugs(Arrays.asList(drugs));

		return this;
	}

	public HealthStatusTransitionMapBuilder ifIsTreatedWithAnyDrug() {
		currentTransitionBuilder
				.effectiveDrugs(Lists.newArrayList());

		return this;
	}

	public HealthStatusTransitionMapBuilder willTransitionToStatus(HealthStatus status) {
		currentTransitionBuilder.resultingStatus(status);

		return this;
	}

	public HealthStatusTransitionMapBuilder withPriority(Integer priority) {
		currentTransitionBuilder.priority(priority);

		return this;
	}

	public HealthStatusTransitionMapBuilder endOfTransition() {
		currentTransitions.add(currentTransitionBuilder.build());
		currentRule.setTransitions(currentTransitions);

		return this;
	}

	public HealthStatusTransitionMapBuilder elseWillTransitionToStatus(HealthStatus status) {
		currentRule.setFallBackStatus(status);

		return this;
	}

	public HealthStatusTransitionMapBuilder endOfRule() {
		if (currentBuildingRuleStatus != null) {
			transitionRules.put(currentBuildingRuleStatus, currentRule);
		} else {
			transitionRules.put(HealthStatus.ANY, currentRule);
		}

		currentTransitionBuilder = HealthStatusTransition.builder();
		currentRule = HealthStatusTransitionRule.builder().build();
		currentTransitions = Lists.newArrayList();

		return this;
	}

	public Map<HealthStatus, HealthStatusTransitionRule> endOfRules() {

		return transitionRules;
	}
}
