package org.stathisdaras.hospital_simulator_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Spring boot application class
 *
 * @author sdaras
 */
@SpringBootApplication
@ComponentScan(basePackages = { "org.stathisdaras.hospital_simulator_api"})
public class HospitalSimulatorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HospitalSimulatorApiApplication.class, args);
	}

}

