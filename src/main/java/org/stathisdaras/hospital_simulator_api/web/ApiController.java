package org.stathisdaras.hospital_simulator_api.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.stathisdaras.hospital_simulator_api.service.HospitalSimulationService;

/**
 * Api controller exposing endpoint methods to be consumed by clients
 *
 * @author sdaras
 */
@RestController
@RequestMapping("/api/hospital-simulator")
public class ApiController extends AbstractController {

	@Autowired
	private HospitalSimulationService service;

	/**
	 * Invokes injected service
	 *
	 * @param statuses
	 * 		comma seperated string of health statuses
	 * @param drugs
	 * 		comma seperated string of medicines
	 * @return
	 * 		result of hospital simulation
	 */
	@GetMapping("/treat-patients/{statuses}/{drugs}")
	public String administerDrugsToPatients(
			@PathVariable String statuses,
			@PathVariable String drugs) {

		return service.administerDrugsToPatients(statuses, drugs);
	}
}
