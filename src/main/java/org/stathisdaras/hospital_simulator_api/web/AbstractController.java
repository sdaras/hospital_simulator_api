package org.stathisdaras.hospital_simulator_api.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.stathisdaras.hospital_simulator_api.exception.ValidationException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract web controller defining core  functionality (e.g exception handling)
 *
 * @author sdaras
 */
public class AbstractController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractController.class);

	private static final String ERROR = "ERROR";

	/**
	 * Handles business Validation Exceptions as 400 error (BAD_REQUEST)
	 */
	@ExceptionHandler(ValidationException.class)
	public ResponseEntity<Map<String, Object>> badRequest(ValidationException e, HttpServletResponse response) throws IOException {

		LOGGER.error("Processed error message: {}", e.getMessage());

		Map<String, Object> responseBody = new HashMap<>();
		responseBody.put("description", e.getMessage());
		responseBody.put("result", ERROR);

		return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
	}

}
