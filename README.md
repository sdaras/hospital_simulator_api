# Hospital Simulator API

Java API, which can simulate the future patients’ state, based on their current state and a list of drugs they take.

Patients can have one of these states:

- F: Fever
- H: Healthy
- D: Diabetes
- T: Tuberculosis
- X: Dead

In the “Hospital simulator” drugs are provided to all patients. It is not possible to target a
specific patient. This is the list of available drugs:

- As: Aspirin
- An: Antibiotic
- I: Insulin
- P: Paracetamol

Drugs can change patients’ states. They can cure, cause side effects or even kill a patient if not properly prescribed.
Drugs effects are described by the following rules:

- Aspirin cures Fever;
- Antibiotic cures Tuberculosis;
- Insulin prevents diabetic subject from dying, does not cure Diabetes;
- If insulin is mixed with antibiotic, healthy people catch Fever;
- Paracetamol cures Fever;
- Paracetamol kills subject if mixed with aspirin;
- One time in a million the Flying Flying Spaghetti Monster shows his noodly power
and resurrects a dead patient (Dead becomes Healthy).

## Input

#### Parameter 1
List of patients' health status codes, separated by a comma. e.g. “D,F,F” means we have 3
patients, one with diabetes and two with fever.

#### Parameter 2
List of drugs codes, separated by a comma, e.g. “As,I” means patients will be treated with
Aspirin and Insulin.

#### Output
The result should be sent to stdout.
It should be a comma separated string with number of patients with a given state, following
the format:
F:NP,H:NP,D:NP,T:NP,X:NP

Where:
- F, H, D, T, X are patients’ health status codes;
- NP is a number of patients for a given state;

E.g. “F:0,H:2,D:0,T:0,X:1” means there are two healthy patients and one that is dead.

## Prerequisite

- Java 8
- Maven 3
- Docker

## Build & Run

In order to build, navigate to project root folder and run:

```
 mvn clean install -DskipTests
```
In order to run the application in a docker container:

-  Create docker image:  

```
 docker build -t hospital_simulator_api .
```

-  Run a container using this image:  

```
 docker run --name hospital_simulator_api -d --rm -it -p 8080:8080 hospital_simulator_api
```

After the above steps, the app will be running in your local machine in path http://localhost:8080/api/hospital-simulator

You may then try to check the program by accessing the swagger ui:

```
 http://localhost:8080/swagger-ui.html
```

or using curl, for example:

```
curl --header "Content-Type: application/json" --request GET http://localhost:8080/api/hospital-simulator/treat-patients/{statuses}/{drugs}
```


## How it works

The application uses a custom transition rule engine implementation to represent the various transitions from one health status to another given the list of 
drugs administered to the patients. 

The implementation is based on the essential assumption that the list of drugs is administered to the patient as a combination, not in a serial way and only
one health status transition will occur at any given time. E.g,  for the following execution:

```
 curl --header "Content-Type: application/json" --request GET http://localhost:8080/api/hospital-simulator/treat-patients/F/P,An,I
```

the output will be:

    H:1,F:0,D:0,T:0,X:0

This means that for status F (Fever), the only effective drug that was found on the list was P (Paracetamol) and the patient transitioned to H (HEALTHY) 
since the drugs were administered as a combination. The fact that after Paracetamol follows a combination of drugs that can make the healthy patient
get Fever again is not taken into consideration, since the drugs are not administered in a serial way.


The configuration of the rule engine is filled in the **HealthStatusTransitionConfiguration** class. A custom builder is used **HealthStatusTransitionMapBuilder**
in order to make the transitions rule engine easy to use. For example, in order to describe the rule *Insulin prevents diabetic subject from dying, does not cure Diabetes*,
the following expression must be added to the configuration class:

```
HealthStatusTransitionMapBuilder.transitionRules()

....

// Insulin prevents diabetic subject from dying, does not cure Diabetes
.forPatientOfStatus(HealthStatus.DIABETES).ifIsTreatedWithEitherOf(Drug.INSULIN).willTransitionToStatus(HealthStatus.DIABETES)
.endOfTransition()
.elseWillTransitionToStatus(HealthStatus.DEAD)
.endOfRule()

.....

.endOfRules();
```

If someone wants to extend this application's functionality by adding more health status codes, drug codes and their corresponding transition rules, all they have to do
is add the proper enumerations and codes in **HealthStatus** and **Drug** enums and then add the corresponding transition rule in the **HealthStatusTransitionConfiguration** class.

The system also supports multiple transitions per health status. And if more than one transitions can be applied, given the administered drugs, then the system will select
the most important using the priority field in HealthStatusTransition class.

For example let's say that we introduce a new drug Adrenaline (Ad) that will make a healthy patient transition to the newly introduced health status Hypertension (Hy).

New Health status:
       
        public enum HealthStatus {

        ...
        
        HYPERTENSION("Hy");

        ...
        }
        



New Drug:
    
        public enum Drug {

        ...
        
        ADRENALINE("Ad");

        ...
        }
        
So, since a healthy patient can transition to fever status by taking insulin mixed with antibiotic, and also results in hypertension by taking adrenaline,
we have to define which transition is more important by introducing the corresponding priority. So if hypertension effect is deemed more important, the configuration 
will be as follows:

```
HealthStatusTransitionMapBuilder.transitionRules()

....

// If insulin is mixed with antibiotic, healthy people catch Fever
.forPatientOfStatus(HealthStatus.HEALTHY).ifIsTreatedWithMixOf(Drug.INSULIN, Drug.ANTIBIOTIC).willTransitionToStatus(HealthStatus.FEVER).withPriority(2)
.endOfTransition()
// If adrenaline is administered, healthy people get hypertension
.forPatientOfStatus(HealthStatus.HEALTHY).ifIsTreatedWithEitherOf(Drug.ADRENALINE).willTransitionToStatus(HealthStatus.HYPERTENSION).withPriority(1)
.endOfTransition()
.elseWillTransitionToStatus(HealthStatus.HEALTHY)
.endOfRule()

.....

.endOfRules();
```

So for the given input:

```
 curl --header "Content-Type: application/json" --request GET http://localhost:8080/api/hospital-simulator/treat-patients/H/An,I,Ad
```

the output will be:

```
   H:0,F:0,D:0,T:0,X:0,Hy:1
```
